AED - Adsorption energy descriptor 

Utilize descriptors used in the past references, including 

- Regular coordination number and generalized coordination number 
- Occupied s and d band center 
- Orbital-wise coordination number  
