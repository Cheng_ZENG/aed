from __future__ import print_function, division
import os 
import sys 
import time
import glob 
import numpy as np 
from math import pi

from ase import Atoms 
from ase.visualize import view 
from ase.parallel import paropen
from ase.data import reference_states
from ase.neighborlist import NeighborList

from gpaw import GPAW
from data import solid_state_data

__author__ = "Cheng Zeng" 
__email__ = "cheng_zeng1@brown.edu"
__copyright__ = "Copyright 2019, Quantitative strain analysis"
__credits__ = "Andrew Peterson, Alireza Khordishi"
__version__ = "0.0.2"

class Descriptor:
	"""Tools to get descriptors for binding energy changes. We hope to seek 
	a universal descriptor which can be employed to construct the scaling 
	relation of descriptor susceptibility to strain and binding energy 
	susceptibility to strain across various models systems (including 
	different sites, adsorbates and surfaces). Here we consider descritors 
	reported in the previvous studies. 

	1. Occupied s/d band center. 
	2. Regular coordination number (RCN).
	3. Generalized coordination number (GCN and SGCN). 
	4. Orbital-wise coordination number (OCN).   
	
	The descriptors are used based on the local chemical environment of the 
	center atom, hence you should be really careful when dealing with peridic
	system with a small-size cell. 

	Parameters 
	-----------
	atoms: object 

	index: int 
		Index of the atom of interest. Note that the current version only 
		considers descriptors related to single atom. 

	lc: float 
		Lattice constants. If not given, use the lattice constants from ase
		databases. 
	
	log: str 
		Log file name to keep track of the flow of messages. 
	"""

	def __init__(self, atoms, index, lc=None, log=None,):
		assert isinstance(atoms, Atoms), "Make sure you input a Atoms object!"
		self.atoms = atoms 
		assert isinstance(index, int), 'Index can only be integer!'
		self.index = index 
		self.lc = lc 
		self.band_center = None 
		self.rcn = None 
		self.gcn = None 
		self.ocn = None 
		self._log = Logger(log) 

	def get_ocn(self, Rc=None): 
		"""get the  orbital coordination number (ocn) given the number 
		of the target atom. 
		The formula to calculate ocn can be found in the paper: 
		https://journals.aps.org/prl/abstract/10.1103/PhysRevLett.118.036101

		Rc: float 
			Cutoff distance, in angstrom. Necessary for ocn. The maximum distance 
			to the center atom at which properties are calculated. 
		"""  
		
		index = self.index 
		# h_bar_square_over_m = 7.62 # h^2/m, in eV-angstrom**2
		atoms = self.atoms.copy() 
		pbc = atoms.pbc 
		cell = atoms.cell 

		atomic_number = atoms.get_atomic_numbers()[index]
		symmetry = reference_states[atomic_number]['symmetry']
		lc = self.lc if self.lc != None else reference_states[atomic_number]['a']
		
		# a0: the shortest interatomic distance in the bulk cell 
		if symmetry == 'fcc': 
			a0 = lc/np.sqrt(2)
		elif symmetry == 'bcc':
			a0 = lc*np.sqrt(3)/2
		elif symmetry == 'hcp':
			a0 = lc
		else:
			raise NotImplementedError('Cells are limited to fcc, bcc and hcp.')
		
		Rc = Rc if Rc != None else 5.5

		axis_div, _ = np.divmod(Rc*np.ones(3), cell.diagonal())
		repeat = (2*np.array(axis_div) + 3)*pbc + ~pbc 
		repeat_list = [int(_) for _ in repeat]
	
		atoms_new = atoms.copy()
		atoms_new = atoms_new.repeat(repeat_list) 
		rep = (repeat-1)/2
		rep = rep[rep!=0] if np.any(rep) else 0
		index = index + len(atoms)*np.product(rep)
		index = int(index)

		d = atoms_new.get_distances(a=index, indices=range(len(atoms_new)),)
		d = np.concatenate([d[:index], d[index+1:]])
		mask = (d <= Rc)
		d = d[mask]
		atom = atoms_new.get_chemical_symbols()[index]
		rd0 = solid_state_data[atom]['rd']
		del atoms_new[index]
		symbols = atoms_new.get_chemical_symbols()
		rds = np.array([solid_state_data[_]['rd'] for _ in symbols])
		rds = rds[mask]
 
		# symmetry dependent coefficient. Data tabulated in the Book: 
		#"W. A. Harrison, Electronic Structure and the Properties of
		#Solids: The Physics of the Chemical Bond (Dover Publica-
		#tions, New York, 1989)." 
		self.eta_ss_sigma = eta_ss_sigma = -1.40  
		self.eta_sd_sigma = eta_sd_sigma = -3.16
		self.eta_dd_sigma = eta_dd_sigma = -16.2
		self.eta_dd_pi = eta_dd_pi = 8.75 
		self.eta_dd_delta = eta_dd_delta = 0

		ss_sigma_squared = eta_ss_sigma**2*sum(1/d**4)
		sd_sigma_squared = eta_sd_sigma**2*sum(rds**3/d**7)
		M_2i_s = ss_sigma_squared + sd_sigma_squared 
		ss_sigma_infty_squared =  eta_ss_sigma**2*(1/a0**4)
		sd_sigma_infty_squared = eta_sd_sigma**2*(rd0**3/a0**7)
		t_infty_squared = ss_sigma_infty_squared + sd_sigma_infty_squared 

		cns = M_2i_s/t_infty_squared
		self.cns = cns 

		dd_sigma_squared = eta_dd_sigma**2*(rd0**3)*sum(rds**3/d**10)
		dd_pi_squared = eta_dd_pi**2*(rd0**3)*sum(rds**3/d**10)
		dd_delta_squared = eta_dd_delta**2*(rd0**3)*sum(rds**3/d**10)
		M_2i_d = sd_sigma_squared + dd_sigma_squared + 2*dd_pi_squared 
		+ 2*dd_delta_squared

		dd_sigma_infty_squared  = eta_dd_sigma**2*(rd0**6/a0**10)
		dd_pi_infty_squared = eta_dd_pi**2*(rd0**6/a0**10)
		dd_delta_infty_squared = eta_dd_delta**2*(rd0**6/a0**10)
		dd_infty_squared = dd_sigma_infty_squared  + 2*dd_pi_infty_squared\
		+ 2*dd_delta_infty_squared 
		t_infty_squared = sd_sigma_infty_squared + dd_infty_squared 
		
		cnd = M_2i_d/t_infty_squared
		self.cnd = cnd 

		return {
		'cns': np.round(cns,2), 
		'cnd': np.round(cnd,2),
		}

	def get_band_center(self, traj=None, spin=False):
		"""Calculate the s and d band center projected on the 
		atom of interest. Here only occupied band centers are
		obtained. Note that the center position should be referenced 
		to the fermi level."""
		index = self.index 

		if traj == None:
			trajs = glob.glob("./*.gpw") 
			assert len(trajs) == 1, \
			"None or More than one gpw files found, please specify one!"
			traj = trajs[0]
		calc = GPAW(traj, txt=None)
		
		ef = calc.get_fermi_level()
		s, d = {}, {}
		s['energies'], s['dos'] = calc.get_orbital_ldos(a=index, spin=spin, 
			angular='s', npts=1001, )
		d['energies'], d['dos'] = calc.get_orbital_ldos(a=index, spin=spin, 
			angular='d', npts=1001, )
		mask_d = (d['energies'] < ef)
		mask_s = (s['energies'] < ef)

		area_d = np.trapz(d['dos'][mask_d], d['energies'][mask_d])
		area_s = np.trapz(s['dos'][mask_s], s['energies'][mask_s])

		center_d = np.trapz(d['dos'][mask_d] * d['energies'][mask_d],
		 d['energies'][mask_d]) / area_d
		self.d_center = center_d - ef 
		center_s = np.trapz(s['dos'][mask_s] * s['energies'][mask_s],
		 s['energies'][mask_s]) / area_s
		self.s_center = center_s - ef 

		return {
		"s_band_center": np.round(center_s - ef ,2), 
		"d_band_center": np.round(center_d - ef,2), 
		}

	def get_rcn_and_gcn(self, Rc=None):
		"""Calculate the regular coordination number, generalized 
		coordination number and coordination number including strain 
		effect (sgcn). How the gcn and sgcn is obtained is detailed in the 
		literatures below. The idea is to find the first nearest neighbors
		of the atom of interest. Use the `Neighborlist` module in ase 
		to create the number of neighboring list.  
		
		"Fast Prediction of Adsorption Properties for Platinum 
		Nanocatalysts with Generalized Coordination Numbers",
		https://doi.org/10.1002/anie.201402958.

		and 
		"Enabling Generalized Coordination Numbers to Describe
		Strain Effects", https://doi.org/10.1002/cssc.201800569

		Rc: float, 
			curoff distance for creating the neighborlist.
		"""

		index = self.index 
		atoms = self.atoms.copy()

		atomic_number = atoms.get_atomic_numbers()[index]
		symmetry = reference_states[atomic_number]['symmetry']
		lc = self.lc if self.lc != None else reference_states[atomic_number]['a']
		
		# a0: the shortest interatomic distance in the bulk cell 
		if symmetry == 'fcc': 
			a0 = lc/np.sqrt(2)
			cn_infty = 12 
		elif symmetry == 'bcc':
			a0 = lc*np.sqrt(3)/2
			cn_infty = 8 
		elif symmetry == 'hcp':
			a0 = lc
			cn_infty = 12 
		else:
			raise NotImplementedError('Only fcc,bcc and hcp cell allowed.')

		Rc = a0/2 + 0.1 if Rc == None else Rc 

		nl = NeighborList(cutoffs=Rc*np.ones(len(atoms)), 
			self_interaction=False, bothways=True)
		nl.update(atoms)
		indices, _ = nl.get_neighbors(index)

		self.rcn = rcn = len(indices)

		cn = [] 
		for i in indices:
			cn.append(len(nl.get_neighbors(i)[0])/cn_infty)
			
		self.gcn = gcn = np.sum(cn) 

		d_list = atoms.get_distances(a=index, indices=indices)
		sgcn = np.sum(a0*np.array(cn)/d_list )
		self.sgcn = sgcn 

		return {
		'rcn': int(rcn), 
		'gcn': np.round(gcn, 2),
		'sgcn': np.round(sgcn, 2),
		} 


## The `Logger` class is by Andy  
class Logger:

    """Logger that can also deliver timing information.

    Parameters
    ----------
    file : str
        File object or path to the file to write to.  Or set to None for
        a logger that does nothing.
    """

    def __init__(self, file):
        if file is None:
            self.file = None
            return
        if isinstance(file, str):
            self.filename = file
            self.file = paropen(file, 'a')
        self.tics = {}

    def tic(self, label=None):
        """Start a timer.

        Parameters
        ----------
        label : str
            Label for managing multiple timers.
        """
        if self.file is None:
            return
        if label:
            self.tics[label] = time.time()
        else:
            self._tic = time.time()

    def __call__(self, message, toc=None, tic=False):
        """Writes message to the log file.

        Parameters
        ---------
        message : str
            Message to be written.
        toc : bool or str
            If toc=True or toc=label, it will append timing information in
            minutes to the timer.
        tic : bool or str
            If tic=True or tic=label, will start the generic timer or a timer
            associated with label. Equivalent to self.tic(label).
        """
        if self.file is None:
            return
        dt = ''
        if toc:
            if toc is True:
                tic = self._tic
            else:
                tic = self.tics[toc]
            dt = (time.time() - tic) / 60.
            dt = ' %.1f min.' % dt
        if self.file.closed:
            self.file = open(self.filename, 'a')
        self.file.write(message + dt + '\n')
        self.file.flush()
        if tic:
            if tic is True:
                self.tic()
            else:
                self.tic(label=tic)
