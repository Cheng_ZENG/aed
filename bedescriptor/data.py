##### Electronic data
# -epsilond(eV): the energy required to remove one d electron 
# -rd(\AA): the spatial extent of d band orbital 
# -kd(\AA^{-1}): parameter which can be related to the average energy of d 
# band relative to the bottom of the s band. 
# -r0(\AA): metallic nearest-neighbor distance.
# -ri(\AA): ionic radii for a cation, if possible to form ionic solids. 

solid_state_data = {
	'Sc': {'epsilond': 9.35,  'rd': 1.24, 'kd': 1.11, 'r0':1.81, }, 
	'Ti': {'epsilond': 11.04, 'rd': 1.08, 'kd': 1.17, 'r0':1.61, }, 
	'V':  {'epsilond': 12.55, 'rd': 0.98, 'kd': 1.21, 'r0':1.49, }, 
	'Cr': {'epsilond': 13.94, 'rd': 0.90, 'kd': 1.22, 'r0':1.42, },
	'Mn': {'epsilond': 15.27, 'rd': 0.86, 'kd': 1.24, 'r0':1.43, }, 
	'Fe': {'epsilond': 16.54, 'rd': 0.80, 'kd': 1.25, 'r0':1.41, },
	'Co': {'epsilond': 17.77, 'rd': 0.76, 'kd': 1.24, 'r0':1.39, }, 
	'Ni': {'epsilond': 18.96, 'rd': 0.71, 'kd': 1.22, 'r0':1.38, }, 
	'Cu': {'epsilond': 20.14, 'rd': 0.67, 'kd': 1.15, 'r0':1.41, },	


	'Y':  {'epsilond': 6.80,  'rd': 1.58, 'kd': 0.99, 'r0':1.99, }, 
	'Zr': {'epsilond': 8.46,  'rd': 1.41, 'kd': 1.02, 'r0':1.77, }, 
	'Nb': {'epsilond': 10.03, 'rd': 1.28, 'kd': 1.04, 'r0':1.62, }, 
	'Mo': {'epsilond': 11.56, 'rd': 1.20, 'kd': 1.04, 'r0':1.55, },
	'Tc': {'epsilond': 13.08, 'rd': 1.11, 'kd': 1.03, 'r0':1.50, }, 
	'Ru': {'epsilond': 14.59, 'rd': 1.05, 'kd': 1.00, 'r0':1.48, },
	'Rh': {'epsilond': 16.16, 'rd': 0.99, 'kd': 0.95, 'r0':1.49, }, 
	'Pd': {'epsilond': 17.66, 'rd': 0.94, 'kd': 0.93, 'r0':1.52, }, 
	'Ag': {'epsilond': 19.21, 'rd': 0.89, 'kd': 0.71, 'r0':1.59, },	


	'Lu': {'epsilond': 6.62,  'rd': 1.58, 'kd': 1.09, 'r0':1.92, }, 
	'Hf': {'epsilond': 8.14,  'rd': 1.44, 'kd': 1.12, 'r0':1.75, }, 
	'Ta': {'epsilond': 9.57,  'rd': 1.34, 'kd': 1.15, 'r0':1.62, }, 
	'W':  {'epsilond': 10.96, 'rd': 1.27, 'kd': 1.16, 'r0':1.56, },
	'Re': {'epsilond': 12.35, 'rd': 1.20, 'kd': 1.15, 'r0':1.52, }, 
	'Os': {'epsilond': 13.73, 'rd': 1.13, 'kd': 1.14, 'r0':1.49, },
	'Ir': {'epsilond': 15.13, 'rd': 1.08, 'kd': 1.10, 'r0':1.50, }, 
	'Pt': {'epsilond': 16.55, 'rd': 1.04, 'kd': 1.07, 'r0':1.53, }, 
	'Au': {'epsilond': 17.98, 'rd': 1.01, 'kd': 0.98, 'r0':1.59, },	
}

####### Atomic radius 

### data excerpted from the following website
### https://www.schoolmykids.com/learn/interactive-periodic-table/atomic-radius-of-all-the-elements/

# unit in pm 
Ra={
	'H': 53, 
	'C': 67, 
	'N': 56, 
	'O': 48, 
	'Sc': 184, 
	'Fe': 156, 
	'Co': 152, 
	'Ni': 149, 
	'Cu': 145, 
	'Zn': 142, 
	'Y':  212, 
	'Ru': 178, 
	'Rh': 173, 
	'Pd': 169, 
	'Ag': 165, 
	'Cd': 161, 
}